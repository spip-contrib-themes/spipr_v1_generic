<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// T
	'theme_generic_description' => 'Thème générique SPIPr pour SPIPr-dist',
	'theme_generic_slogan' => 'Thème générique SPIPr pour SPIPr-dist',
);
